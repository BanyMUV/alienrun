﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private float impulse = 8f;

    private bool isHit = false;

    public GameObject drop;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player" && !isHit)
        {
            collision.gameObject.GetComponent<Player>().RecountHp(-1);

            collision.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * impulse, ForceMode2D.Impulse);
        }
    }

    private IEnumerator Death()
    {
        if(drop != null)
        {
            Instantiate(drop, transform.position, Quaternion.identity);//Що викидає, позиція, угол повороту
        }
        isHit = true;

        GetComponent<Animator>().SetBool("Death", true);
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        GetComponent<Collider2D>().enabled = false;

        transform.GetChild(0).GetComponent<Collider2D>().enabled = false;

        yield return new WaitForSeconds(2f);

        Destroy(gameObject);
    }

    public void StartDeath()
    {
        StartCoroutine(Death());
    }
}
