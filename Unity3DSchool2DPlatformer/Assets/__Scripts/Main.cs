﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum TimeWork
{
    None,
    Stopwatch,
    Timer
}

public class Main : MonoBehaviour
{
    public Player player;
    public Text coinText;
    public Image[] hearts;
    public Sprite isLife, nonLife;
    public GameObject pauseScreen;
    public GameObject winScreen;
    public GameObject loseScreen;
    public Text timeText;
    public TimeWork timeWork;
    public float countdown;
    public GameObject inventoryPanel;
    public Soundeffector soundeffector;
    public AudioSource musicSource, soundSource;

    private float timer = 0f;

    private void Start()
    {
        musicSource.volume = (float)PlayerPrefs.GetInt("MusicVolume") / 9;
        soundSource.volume = (float)PlayerPrefs.GetInt("SoundVolume") / 9;

        if ((int)timeWork == 2)//(int)timeWork == 2, int для сравнения с целочисленім типом проверяем включен ли режим timer
        {
            timer = countdown;
        }
    }
    public void ReloadLevel()
    {
        Time.timeScale = 1f;
        player.enabled = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Update()
    {
        coinText.text = player.GetCoins().ToString();

        for(int i = 0; i < hearts.Length; i++)
        {
            if(player.GetHP() > i)
            {
                hearts[i].sprite = isLife;
            }
            else
            {
                hearts[i].sprite = nonLife;
            }
        }

        if((int)timeWork == 1)
        {
            timer += Time.deltaTime;
            timeText.text = timer.ToString("F1").Replace(",", ":");//F2 два знаки після коми, релейс "," міняємо на ":"
        }
        else if((int) timeWork == 2)
        {
            timer -= Time.deltaTime;
            //timeText.text = timer.ToString("F1").Replace(",", ":");
            timeText.text = ((int)timer / 60).ToString() + ":" + ((int)timer - ((int) timer / 60) * 60).ToString("D2");
            if (timer <= 0)
            {
                Lose();
            }
        }
        else
        {
            timeText.gameObject.SetActive(false);
        }
        
    }

    public void PauseOn()
    {
        Time.timeScale = 0f;// время течение
        player.enabled = false;
        pauseScreen.SetActive(true); 
    }

    public void PauseOff()
    {
        Time.timeScale = 1f;
        player.enabled = true;
        pauseScreen.SetActive(false);
    }

    public void Win()
    {
        soundeffector.PlayWinSound();
        Time.timeScale = 0f;
        player.enabled = false;
        winScreen.SetActive(true);

        if (!PlayerPrefs.HasKey("Levels") || PlayerPrefs.GetInt("Levels") < SceneManager.GetActiveScene().buildIndex)// перший раз в грі ключа ще не існує, ключ вже є але запишеться тільки тоді якщо рівень ще не пройдено
        {
            PlayerPrefs.SetInt("Levels", SceneManager.GetActiveScene().buildIndex);
        }

        if (PlayerPrefs.HasKey("Coins"))
        {
            PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") + player.GetCoins());
        }
        else
        {
            PlayerPrefs.SetInt("Coins", player.GetCoins());
        }

        inventoryPanel.SetActive(false);
        GetComponent<Inventory>().RecountItems();
    }

    public void Lose()
    {
        soundeffector.PlayLoseSound();
        Time.timeScale = 0f;
        player.enabled = false;
        loseScreen.SetActive(true);

        inventoryPanel.SetActive(false);
        GetComponent<Inventory>().RecountItems();
    }

    public void MenuLevel()
    {
        Time.timeScale = 1f;
        player.enabled = true;
        SceneManager.LoadScene("MainMenu");
    }

    public void NextLevel()
    {
        Time.timeScale = 1f;
        player.enabled = true;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}