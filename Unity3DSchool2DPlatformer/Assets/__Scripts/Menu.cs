﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public Button[] levels;
    public Text coinText;
    public Slider musicSlider, soundSlider;
    public Text musicText, soundText;

    void Start()
    {
        if (PlayerPrefs.HasKey("Levels"))
        {
            for(int i =0; i < levels.Length; i++)
            {
                if (i <= PlayerPrefs.GetInt("Levels"))
                {
                    levels[i].interactable = true;
                }
                else
                {
                    levels[i].interactable = false;
                }
            }
        }

        if (!PlayerPrefs.HasKey("hp"))
        {
            PlayerPrefs.SetInt("hp", 0);
        }
        if (!PlayerPrefs.HasKey("blueGem"))
        {
            PlayerPrefs.SetInt("blueGem", 0);
        }
        if (!PlayerPrefs.HasKey("greenGem"))
        {
            PlayerPrefs.SetInt("greenGem", 0);
        }

        if (!PlayerPrefs.HasKey("MusicVolume"))
        {
            PlayerPrefs.SetInt("MusicVolume", 3);
        }
        if (!PlayerPrefs.HasKey("SoundVolume"))
        {
            PlayerPrefs.SetInt("SoundVolume", 5);
        }

        musicSlider.value = PlayerPrefs.GetInt("MusicVolume");
        soundSlider.value = PlayerPrefs.GetInt("SoundVolume");
    }


    void Update()
    {
        PlayerPrefs.SetInt("MusicVolume", (int)musicSlider.value);
        PlayerPrefs.SetInt("SoundVolume", (int)soundSlider.value);

        musicText.text = musicSlider.value.ToString();
        soundText.text = soundSlider.value.ToString();

        if (PlayerPrefs.HasKey("Coins"))
        {
            coinText.text = PlayerPrefs.GetInt("Coins").ToString();
        }
        else
        {
            coinText.text = "0";
        }
    }

    public void OpenScene(int index)
    {
        SceneManager.LoadScene(index);
    }

    public void DelKey()
    {
        PlayerPrefs.DeleteAll();
    }

    public void Buy_hp(int cost)
    {
        if(PlayerPrefs.GetInt("Coins") >= cost)
        {
            PlayerPrefs.SetInt("hp", PlayerPrefs.GetInt("hp") + 1);
            PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - cost);
        }
    }

    public void Buy_blueGem(int cost)
    {
        if (PlayerPrefs.GetInt("Coins") >= cost)
        {
            PlayerPrefs.SetInt("blueGem", PlayerPrefs.GetInt("blueGem") + 1);
            PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - cost);
        }
    }

    public void Buy_greenGem(int cost)
    {
        if (PlayerPrefs.GetInt("Coins") >= cost)
        {
            PlayerPrefs.SetInt("greenGem", PlayerPrefs.GetInt("greenGem") + 1);
            PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - cost);
        }
    }
}
