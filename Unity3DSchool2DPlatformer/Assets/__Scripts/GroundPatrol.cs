﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundPatrol : MonoBehaviour
{
    public float speed = 1.5f;
    public bool moveLeft = true;

    [SerializeField]
    private Transform groundDetect;

    void Update()
    {
        transform.Translate(Vector2.left  * speed * Time.deltaTime);
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetect.position, Vector2.down, 1f);//откуда исходит луч, куда направлен луч, длина луча

        if(groundInfo.collider == false)
        {
            if(moveLeft == true)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);// eulerAnger значение в градусах угол Rotation
                moveLeft = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);// eulerAnger значение в градусах угол Rotation
                moveLeft = true;
            }
        }
    }
}
